# Twenty something

## Summary

A short collection of tests to experiment with new and incubating features of Java 20 (and 19-) 

## Covered

### Concurrency:
  - ScopedValues as an alternative to ThreadLocals [JEP-429](https://openjdk.org/jeps/429)
  - Structured concurrency as a way to simplify multithreaded programming [JEP-437](https://openjdk.org/jeps/437)
  - Virtual Threads [JEP-436](https://openjdk.org/jeps/436)
### Pattern matching 
  - instanceof Operator and switch expressions [JEP-433](https://openjdk.org/jeps/433) 
  - Record patterns and record deconstructing [JEP-432](https://openjdk.org/jeps/432)

## Not covered 

- Vector API [JEP-438](https://openjdk.org/jeps/438)
- Foreign Function API [JEP-434](https://openjdk.org/jeps/434)
