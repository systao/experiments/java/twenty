module fr.systao.experiments.twenty {
    requires jdk.incubator.concurrent;
    requires org.junit.jupiter.api;

    exports fr.systao.experiments.twenty.patterns;
    exports fr.systao.experiments.twenty.model;
    exports fr.systao.experiments.twenty.concurrency;

    opens fr.systao.experiments.twenty.patterns;
    opens fr.systao.experiments.twenty.model;
    opens fr.systao.experiments.twenty.concurrency;
}