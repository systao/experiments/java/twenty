package fr.systao.experiments.twenty.patterns;

import fr.systao.experiments.twenty.model.PublicationFactory;
import fr.systao.experiments.twenty.model.Author;
import fr.systao.experiments.twenty.model.Book;
import fr.systao.experiments.twenty.model.Genre;
import fr.systao.experiments.twenty.model.Publication;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class RecordPatternsExample {
    private String originalTitle = "En man som heter Ove";
    private int publishingYear = 2015;
    private String authorFirstName = "Fredrik";
    private String authorLastName = "Backman";

    @Test
    void example() {
        Publication p = new PublicationFactory().getPublication(originalTitle, publishingYear, authorFirstName, authorLastName, Genre.FICTION);

        // RecordPattern feature in a nutshell: test that p is instance of Book AND deconstruct it
        if ( p instanceof Book(var title, var year, Author(var firstName, var lastName), var genre) )  {
            Assertions.assertEquals(originalTitle, title);
            Assertions.assertEquals(publishingYear, year);
            Assertions.assertEquals(authorFirstName, firstName);
            Assertions.assertEquals(authorLastName, lastName);
            Assertions.assertEquals(Genre.FICTION, genre);
        }
    }
}

