package fr.systao.experiments.twenty.patterns;

import fr.systao.experiments.twenty.model.PublicationFactory;
import fr.systao.experiments.twenty.model.Book;
import fr.systao.experiments.twenty.model.Genre;
import fr.systao.experiments.twenty.model.Publication;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PatternMatchingExample {
    @Test
    void example() {
        Publication p = new PublicationFactory().getPublication("Elektra", 2022, "Jennifer", "Saint", Genre.HISTORICAL);

        // PatternMatchnig feature in a nutshell: assign a new typed local variable and get rid of casts
        if (p instanceof Book b) {
            Assertions.assertEquals("Elektra", b.title());
        }

        // can also be used in a switch
        switch ( p ) {
            case Book b: Assertions.assertEquals("Elektra", b.title());
            default: ;
        }

    }
}
