package fr.systao.experiments.twenty.model;

import fr.systao.experiments.twenty.model.Author;
import fr.systao.experiments.twenty.model.Book;
import fr.systao.experiments.twenty.model.Genre;
import fr.systao.experiments.twenty.model.Publication;

public class PublicationFactory {
    private Publication[] stock = {
            getPublication("Elektra", 2022, "Jennifer", "Saint", Genre.HISTORICAL),
            getPublication("En man som heter Ove", 2015, "Fredrik", "Backman", Genre.FICTION),
    };

    public Publication getPublication(String title, int year, String firstName, String lastName, Genre  genre) {
        return new Book(title, year, new Author(firstName, lastName), genre);
    }

    public Publication[] getStock() {
        return stock;
    }
}
