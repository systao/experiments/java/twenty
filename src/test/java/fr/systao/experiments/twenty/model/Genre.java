package fr.systao.experiments.twenty.model;

public enum Genre {
    FICTION,
    HISTORICAL
}
