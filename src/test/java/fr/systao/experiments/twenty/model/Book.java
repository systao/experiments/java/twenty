package fr.systao.experiments.twenty.model;

public record Book(String title, int year, Author author, Genre genre) implements Publication {

    public String toString() {
        return String.format("%s, %d, %s, %s", title, year, author, genre);
    }

}
