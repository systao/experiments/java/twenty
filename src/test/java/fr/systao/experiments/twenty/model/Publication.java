package fr.systao.experiments.twenty.model;

public interface Publication {
    String title();
}
