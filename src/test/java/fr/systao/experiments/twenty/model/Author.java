package fr.systao.experiments.twenty.model;

public record Author(String firstName, String lastName) {
    public String toString() {
        return String.format("%s %s", firstName, lastName);
    }
}
