package fr.systao.experiments.twenty.concurrency;

import fr.systao.experiments.twenty.model.Publication;
import fr.systao.experiments.twenty.model.PublicationFactory;
import jdk.incubator.concurrent.ScopedValue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

public class ScopedValuesTest {

    private static ScopedValue<Publication> publication = ScopedValue.newInstance();

    @Test
    void example() throws InterruptedException {
        PublicationFactory factory = new PublicationFactory();

        Assertions.assertThrowsExactly(NoSuchElementException.class, () -> publication.get());

        // ScopedValues feature in a nutshell: immutable values passed down a runnable
        // can be used instead of ThreadLocal. ScopedValues are... scoped!
        ScopedValue.where(publication, factory.getStock()[0]).run(getRunnable("Elektra", 0));
        ScopedValue.where(publication, factory.getStock()[1]).run(getRunnable("En man som heter Ove", 100));

        Assertions.assertThrowsExactly(NoSuchElementException.class, () -> publication.get());
    }

    private Runnable getRunnable(String title, int sleepDurationMs) {
        return () -> {
            try {
                Thread.sleep(sleepDurationMs);
                Assertions.assertEquals(title, publication.get().title());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        };
    }

}
