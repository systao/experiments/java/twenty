package fr.systao.experiments.twenty.concurrency;

import org.junit.jupiter.api.Test;

public class VirtualThreadsTest {

    @Test
    void example() {
        try {
            // In a nutshell, virtual threads are lightweight threads that is not tied to a specific OS thread

            // virtual thread
            Thread.Builder builder = Thread.ofVirtual().name("worker-", 0);
            Thread virtual = builder.start(() -> {
                System.out.println("Thread ID: " + Thread.currentThread().threadId());
            });
            virtual.join();

            // OS thread
            Thread
                    .ofPlatform()
                    .name("worker-", 1)
                    .start(() -> System.out.println("Thread ID: " + Thread.currentThread().threadId()))
                    .join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
