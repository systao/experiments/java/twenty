package fr.systao.experiments.twenty.concurrency;

import fr.systao.experiments.twenty.model.Publication;
import fr.systao.experiments.twenty.model.PublicationFactory;
import jdk.incubator.concurrent.StructuredTaskScope;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

public class StructuredConcurrencyTest {

    private static String BARNES = "Barnes and noble";
    private static String AMAZON = "Amazon";

    private PublicationFactory factory;

    private Map<String, List<Publication>> sources;
    @BeforeEach
    void setUp() {
        factory = new PublicationFactory();
        sources = Map.of(
                BARNES, List.of(factory.getStock()[0]),
                AMAZON, List.of(factory.getStock()[1]));
    }



    @Test
    void example() throws Exception {

        List<Publication> publications = new ArrayList<>();

        // StructureConcurrency in a nutshell: simplify multithreaded programming
        try (var scope = new StructuredTaskScope.ShutdownOnFailure()) {
            List<Future<List<Publication>>> futures = new ArrayList<>();
            for (var store : sources.keySet() ) {
                futures.add(scope.fork(() -> lookup(store)));
            }

            scope.join();
            scope.throwIfFailed();

            for (var future : futures ) {
                publications.addAll(future.resultNow());
            }
        }

        Assertions.assertTrue(publications.contains(factory.getStock()[0]) &&
                        publications.contains(factory.getStock()[1]));


    }

    private List<Publication> lookup(String source) {
        return sources.get(source);
    }
}
